﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Manager.user_class
{
    public class user_tup
    {
        int id;
        string name;
        int user_id;
        string password;
        bool teacher;
        string profileImg;
        bool admin;

        public user_tup(int id, string name, int user_id, string password, bool teacher, string profileImg, bool admin)
        {
            this.id = id;
            this.name = name;
            this.user_id = user_id;
            this.password = password;
            this.teacher = teacher;
            this.profileImg = profileImg;
            this.admin = admin;
        }

        public bool Admin
        {
            get { return this.admin; }
            set { this.admin = value; }
        }

        public string ProfileImg
        {
            get { return this.profileImg; }
            set { this.profileImg = value; }
        }

        public int Id
        {
            get { return this.id; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int User_id
        {
            get { return this.user_id; }
            set { this.user_id = value; }
        }

        public string Password
        {
            get { return this.password; }
            set { this.password = value; }
        }

        public bool Teacher
        {
            get { return this.teacher; }
            set { this.teacher = value; }
        }

    }
}