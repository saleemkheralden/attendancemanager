﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Manager.templates
{
    public partial class schedule_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
                Response.Redirect("login_page.aspx");

            this.user_name.Text = ((user_class.user_tup)(Session["user"])).Name;
            string date = DateTime.Now.ToString("yyyy-MM-dd");

            Dictionary<int, string> dict = ((Dictionary<int, string>)(Session["class"]));
            Dictionary<int, string> sub_dict = ((Dictionary<int, string>)(Session["subject"]));


            if (!IsPostBack)
            {

                string sub_qry = "SELECT * FROM subjects_tbl WHERE teacher_id=" + ((user_class.user_tup)(Session["user"])).User_id;
                DataSet sub_ds = SQL_Wrapper.SQL.SELECT(sub_qry, "subjects_tbl");

                for (int i = 0; i < sub_ds.Tables["subjects_tbl"].Rows.Count; i++)
                {
                    ListItem item = new ListItem(sub_dict[int.Parse(sub_ds.Tables["subjects_tbl"].Rows[i][1].ToString())], sub_ds.Tables["subjects_tbl"].Rows[i][1].ToString());
                    this.subject_droplist.Items.Add(item);
                }



                foreach (int key in dict.Keys)
                {
                    ListItem item = new ListItem(dict[key], key.ToString());
                    this.classes_droplist.Items.Add(item);
                }

                this.classes_droplist.SelectedValue = "101";

                for (int i = 0; i < 7; i++)
                {
                    ListItem item = new ListItem(DateTime.Now.AddDays((double)i).ToString("yyyy-MM-dd"), DateTime.Now.AddDays((double)i).ToString("yyyy-MM-dd"));
                    this.date_droplist.Items.Add(item);
                }

                this.date_droplist.SelectedValue = date;

                load(date);
            }

        }

        protected void load(string date)
        {
            string tbl = "lessons_tbl";
            string qry = "SELECT * FROM lessons_tbl WHERE teacher_id=" + ((user_class.user_tup)(Session["user"])).User_id + " AND date='" + date + "' ORDER BY lesson_num ASC";
            DataSet list = SQL_Wrapper.SQL.SELECT(qry, "lessons_tbl");

            if (list != null)
                for (int i = 0; i < list.Tables[tbl].Rows.Count; i++)
                {
                    TableRow trow = new TableRow();
                    TableCell _class = new TableCell();
                    TableCell lesson = new TableCell();
                    TableCell subject = new TableCell();

                    //TableCell _date = new TableCell();

                    _class.Text = ((Dictionary<int, string>)(Session["class"]))[int.Parse(list.Tables[tbl].Rows[i]["class"].ToString())];
                    lesson.Text = list.Tables[tbl].Rows[i]["lesson_num"].ToString();
                    subject.Text = ((Dictionary<int, string>)(Session["subject"]))[int.Parse(list.Tables[tbl].Rows[i]["subject"].ToString())];

                    //_class.Text = list.Tables[tbl].Rows[0][""];

                    trow.Controls.Add(_class);
                    trow.Controls.Add(subject);
                    trow.Controls.Add(lesson);
                    //trow.Controls.Add(_date);
                    this.lessons.Rows.Add(trow);
                }
        }

        protected void date_droplist_SelectedIndexChanged(object sender, EventArgs e)
        {
            load(this.date_droplist.SelectedValue);
        }

        protected void add_lesson_btn_Click(object sender, EventArgs e)
        {
            try
            {
                string _class = this.classes_droplist.SelectedValue;
                int lesson = int.Parse(this.lesson_num.Value);
                int subject = int.Parse(this.subject_droplist.SelectedValue);
                string select_qry = "SELECT * FROM lessons_tbl WHERE lesson_num={0} AND date='{1}'";
                select_qry = string.Format(select_qry, lesson, this.date_droplist.SelectedValue);

                DataSet ds = SQL_Wrapper.SQL.SELECT(select_qry, "lessons_tbl");

                if (ds.Tables["lessons_tbl"].Rows.Count == 0)
                {
                    string qry = "INSERT INTO lessons_tbl (teacher_id, class, lesson_num, date, subject) VALUES ({0}, {1}, {2}, '{3}', {4})";
                    qry = string.Format(qry, ((user_class.user_tup)(Session["user"])).User_id, _class, lesson, this.date_droplist.SelectedValue, this.subject_droplist.SelectedValue);

                    SQL_Wrapper.SQL.INSERT(qry);
                    
                    string select_sc = "SELECT * FROM weekly_sc_tbl WHERE id=" + _class + " AND lesson=" + lesson;
                    DataSet sc_ds = SQL_Wrapper.SQL.SELECT(select_sc, "weekly_sc_tbl");

                    if (sc_ds.Tables["weekly_sc_tbl"].Rows.Count == 0)
                    {
                        string day = DateTime.Parse(this.date_droplist.SelectedValue).DayOfWeek.ToString().ToLower();
                        string schedule_qry = "INSERT INTO weekly_sc_tbl (id, lesson, " + day + " ) VALUES ({0}, {1}, '{2}')";
                        schedule_qry = string.Format(schedule_qry, _class, lesson, this.subject_droplist.SelectedItem);

                        SQL_Wrapper.SQL.INSERT(schedule_qry);
                    }
                    else
                    {
                        string day = DateTime.Parse(this.date_droplist.SelectedValue).DayOfWeek.ToString().ToLower();
                        string schedule_qry = "UPDATE weekly_sc_tbl SET " + day + "='{2}' WHERE id={0} AND lesson={1}";
                        schedule_qry = string.Format(schedule_qry, _class, lesson, this.subject_droplist.SelectedItem);

                        SQL_Wrapper.SQL.INSERT(schedule_qry);
                    }

                }
                else
                {
                    this.err_lbl.Text = "יש כבר שיעור קיים בשעה זו";
                    this.err_lbl.ForeColor = System.Drawing.Color.Red;
                }

                load(this.date_droplist.SelectedValue);
            }
            catch
            {
                this.err_lbl.Text = "נסה שוב";
                load(DateTime.Now.ToString("yyyy-MM-dd"));
            }


        }

        protected void del_lesson_btn_Click(object sender, EventArgs e)
        {
            string _class = this.classes_droplist.SelectedValue;
            int lesson = int.Parse(this.lesson_num.Value);

            string qry = "DELETE FROM lessons_tbl WHERE teacher_id={0} AND class={1} AND lesson_num={2} AND date='{3}'";
            qry = string.Format(qry, ((user_class.user_tup)(Session["user"])).User_id, _class, lesson, this.date_droplist.SelectedValue);

            SQL_Wrapper.SQL.DELETE(qry);

            load(this.date_droplist.SelectedValue);
        }
    }
}