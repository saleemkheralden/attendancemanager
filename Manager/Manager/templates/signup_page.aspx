﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="signup_page.aspx.cs" Inherits="Manager.signup_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sign up</title>
    
    <script src="../static/javascript/SignupImgDisplay.js"></script>

    <link href="../static/css/signup_page_stylesheet.css" rel="stylesheet" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


    <style type="text/css">
        
        body {
            text-align: center;
        }

    </style>

</head>
<body>
    <form id="signup_form" runat="server" enctype="multipart/form-data">
        <div>
            <br />
            <div id="img" runat="server" onclick="triggerClick()"></div>
            <asp:FileUpload ID="profileImage" onchange="displayImg(this)" runat="server" style="display: none;"/>
            <br />
            <br />
            <asp:TextBox ID="firstname_textbox" placeholder="שם" runat="server" required="true" ></asp:TextBox>


            <br />
            <br />
            <asp:TextBox ID="id_number_textbox" placeholder="תעודת זהות" runat="server" required="true" ></asp:TextBox>


            <br />
            <br />
            <asp:TextBox ID="password_textbox" placeholder="סיסמה" runat="server" required="true" TextMode="Password" ></asp:TextBox>


            <br />
            <br />
            <asp:TextBox ID="password_conf_textbox" placeholder="סיסמה" runat="server" required="true" TextMode="Password"></asp:TextBox>


            <br />


            <br />
            <asp:CheckBox ID="teacher_checkbox" runat="server" Text="Teacher" AutoPostBack="true" OnCheckedChanged="teacher_checkbox_CheckedChanged" />
            <br />
            <br />
            <center>
                <asp:CheckBoxList ID="subject_list" runat="server" ></asp:CheckBoxList>
            </center>
            <br />
            <br />
            <asp:Button ID="signup_btn" runat="server" OnClick="signup_btn_Click" Text="הרשם" Width="67px" />
            <br />
            <br />
            <asp:Label ID="error_msg_label" runat="server" ForeColor="#E10000"></asp:Label>
            <br />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name required" ControlToValidate="firstname_textbox"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="ID required" ControlToValidate="id_number_textbox"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Password required" ControlToValidate="password_textbox"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Confirm password required" ControlToValidate="password_conf_textbox"></asp:RequiredFieldValidator>

            
        </div>
    </form>
</body>
</html>
