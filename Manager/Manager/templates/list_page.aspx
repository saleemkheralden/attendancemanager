﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list_page.aspx.cs" Inherits="Manager.templates.list_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <link rel="stylesheet" href="../static/css/list_page_stylesheet.css" type="text/css" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


</head>
<body>
    <form id="form1" runat="server">
        <div>

            <ul>
                <li>
                    <a href="main_page.aspx">חזור לדף בית</a>
                </li>

                <li style="float:left">
                    <asp:Label ID="date_lbl" runat="server" Text=""></asp:Label>
                </li>
                
                <li style="float:left">
                    <asp:Label ID="class_lbl" runat="server" Text=""></asp:Label>
                </li>
            </ul>


                <asp:GridView ID="stu_tbl_gridView" runat="server" AllowSorting="True" CssClass="table table-condensed table-hover" AutoGenerateColumns="False" OnRowCancelingEdit="GridView_RowCancelEditing" OnRowEditing="GridView_RowEditing" OnRowUpdating="GridView_RowUpdating">

            <Columns>
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="last_name" HeaderText="last name" SortExpression="last_name" />
                <asp:BoundField DataField="stu_id" HeaderText="id" SortExpression="stu_id" />
                <asp:CheckBoxField DataField="" HeaderText="" SortExpression="date" />
                <%--<asp:BoundField DataField="" HeaderText="22/2/2020" SortExpression="date" />--%>
                <asp:CommandField ShowEditButton="true" />  
                <asp:CommandField ShowDeleteButton="true" />
            </Columns>
        </asp:GridView>
        <asp:Label ID="update_check" runat="server"></asp:Label>
        <%--<asp:BoundField DataField="" HeaderText="22/2/2020" SortExpression="date" />--%>



            <%--<asp:Table ID="stu_lst" runat="server" class="table table-hover">

                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>
                        name
                    </asp:TableHeaderCell>
                    
                    <asp:TableHeaderCell>
                        last name
                    </asp:TableHeaderCell>

                    <asp:TableHeaderCell>
                        class
                    </asp:TableHeaderCell>

                    <asp:TableHeaderCell>
                        id
                    </asp:TableHeaderCell>

                    <asp:TableHeaderCell>
                        העדרות
                    </asp:TableHeaderCell>
                    
                    <asp:TableHeaderCell>
                        איחור
                    </asp:TableHeaderCell>

                </asp:TableHeaderRow>

            </asp:Table>--%>

            <br />
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server">
            </asp:GridView>

        </div>


        

        
        


    </form>
</body>
</html>
