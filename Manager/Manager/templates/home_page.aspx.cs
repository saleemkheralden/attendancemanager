﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Manager.templates
{
    public partial class home_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Dictionary<int, string> class_label = new Dictionary<int, string>();
            string class_label_qry = "SELECT * FROM class_label_tbl";
            string table = "class_label_tbl";
            DataSet ds = SQL_Wrapper.SQL.SELECT(class_label_qry, table);

            for (int i = 0; i < ds.Tables[table].Rows.Count; i++)
            {
                int key = int.Parse(ds.Tables[table].Rows[i][0].ToString());
                string val = ds.Tables[table].Rows[i][1].ToString();
                class_label.Add(key, val);
                Session["class"] = class_label;
            }


            Dictionary<int, string> subject_label = new Dictionary<int, string>();
            string subject_label_qry = "SELECT * FROM subjects_label_tbl";
            string sub_table = "subjects_label_tbl";
            DataSet sub_ds = SQL_Wrapper.SQL.SELECT(subject_label_qry, sub_table);

            for (int i = 0; i < sub_ds.Tables[sub_table].Rows.Count; i++)
            {
                int key = int.Parse(sub_ds.Tables[sub_table].Rows[i][0].ToString());
                string val = sub_ds.Tables[sub_table].Rows[i][1].ToString();
                subject_label.Add(key, val);
                Session["subject"] = subject_label;
            }



        }

    }
}