﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main_page.aspx.cs" Inherits="Manager.templates.main_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attendance</title>

    <link href="../static/css/main_page_stylesheet.css" type="text/css" rel="stylesheet" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


</head>
<body>


    <form id="form1" runat="server">
        <div>
            
            <div class="top-navbar">
                   <ul id="navbar" runat="server">
                       <li id="name_li">
                           <asp:Label ID="user_name" runat="server" Text=""></asp:Label>
                       </li>



                       <li style="float: right;"><a href="logout_page.aspx">Logout</a></li>

                       <li style="float: right;">
                           <asp:HyperLink NavigateUrl="schedule_page.aspx" runat="server" ID="nextWeek_link" Text="Add schedule"></asp:HyperLink>
                       </li>
                       
                       <li style="float: right">
                           <asp:HyperLink NavigateUrl="profile_page.aspx" runat="server" ID="profile_link" Text="Profile"></asp:HyperLink>
                       </li>

                   </ul>
               </div>
            <br />
            <asp:Table ID="Lessons_tbl" runat="server" class="table table-hover">

                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>
                        lesson
                    </asp:TableHeaderCell>
                    
                    <asp:TableHeaderCell>
                        Subject
                    </asp:TableHeaderCell>

                    <asp:TableHeaderCell ID="class_hc">
                        Class
                    </asp:TableHeaderCell>
                </asp:TableHeaderRow>
                


            </asp:Table>
            <br />
            
            <br />
            <br />
            <br />

        </div>
    </form>
</body>
</html>



            <%--<asp:Table ID="Table1" class="table table-hover" runat="server" Width="244px">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>
                        lesson
                    </asp:TableHeaderCell>
                    
                    <asp:TableHeaderCell>
                        class
                    </asp:TableHeaderCell>

                </asp:TableHeaderRow>

                <asp:TableRow>
                    <asp:TableCell>
                        1.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson1_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        2.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson2_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        3.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson3_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        4.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson4_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        5.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson5_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        6.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson6_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        7.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson7_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>

                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        8.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson8_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>

                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        9.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson9_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>

                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        10.
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:DropDownList ID="Lesson10_dropdown" runat="server">
                            <asp:ListItem>
                                בחר
                            </asp:ListItem>
                            
                            <asp:ListItem>
                                י 1
                            </asp:ListItem>

                            <asp:ListItem>
                                י 2
                            </asp:ListItem>

                            <asp:ListItem>
                                י 3
                            </asp:ListItem>

                            <asp:ListItem>
                                י 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יא 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 1
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 2
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 3
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 4
                            </asp:ListItem>

                            <asp:ListItem>
                                יב 5
                            </asp:ListItem>
                        </asp:DropDownList>

                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>--%>



