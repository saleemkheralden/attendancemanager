﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager
{
    public partial class login_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["user"] = null;
        }

        protected void login_btn_Click(object sender, EventArgs e)
        {
            string username = this.username_textbox.Text;
            string password = this.password_textbox.Text;

            if (username.Length != 9)
            {
                //return error msg
                this.errorMsgLabel.Text = "Length of username must be 9";
            }
            else
            {
                string qry = "SELECT * FROM user_tbl WHERE user_id='" + username + "' AND password='" + password + "'";

                DataSet ds = SQL_Wrapper.SQL.SELECT(qry, "user_tbl");


                if (ds.Tables["user_tbl"].Rows.Count == 0)
                {
                    this.errorMsgLabel.Text = "Error username or password";
                }
                else
                {
                    int id = (int)ds.Tables["user_tbl"].Rows[0]["id"];
                    bool teacher = (bool)ds.Tables["user_tbl"].Rows[0]["teacher"];
                    int user_id = (int)ds.Tables["user_tbl"].Rows[0]["user_id"];
                    string _password = (string)ds.Tables["user_tbl"].Rows[0]["password"];
                    string name = (string)ds.Tables["user_tbl"].Rows[0]["firstname"];
                    bool admin = (bool)(ds.Tables["user_tbl"].Rows[0]["admin"]);
                    string profileImg;
                    try
                    {
                        profileImg = (string)ds.Tables["user_tbl"].Rows[0]["profileImg"];
                    }
                    catch
                    {
                        profileImg = "~/UserProfileImages/avatar.png";
                    }
                    user_class.user_tup user = new user_class.user_tup(id, name, user_id, _password, teacher, profileImg, admin);

                    Session["user"] = user;
                    Response.Redirect("main_page.aspx");
                }
            }

        }

    }
}