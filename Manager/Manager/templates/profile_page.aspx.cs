﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Manager.templates
{
    public partial class profile_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
                Response.Redirect("login_page.aspx");
            if (!IsPostBack)
                load();
        }

        protected void load()
        {
            Image image = new Image();
            image.ImageUrl = ((user_class.user_tup)(Session["user"])).ProfileImg;
            image.Height = 100;
            image.Width = 100;
            image.ID = "profileDisplay";


            this.img.Controls.Add(image);

            this.date.Text = DateTime.Now.ToString("dd-MM-yyyy");
            this.name_text.Text = ((user_class.user_tup)(Session["user"])).Name;
            this.id_text.Text = ((user_class.user_tup)(Session["user"])).User_id.ToString();
            this.password_text.Text = ((user_class.user_tup)(Session["user"])).Password;
            
        }

        protected void save_btn_Click(object sender, EventArgs e)
        {

            string fileImg;
            if (this.profileImage.PostedFile != null)
            {
                string path = Path.GetExtension(this.profileImage.PostedFile.FileName);
                if (path != ".jpg" && path != ".jpeg" && path != ".gif" && path != ".png")
                {
                    this.error_lbl.Text = "Only these image types allowed (.jpg, .jpeg, .gif, .png)";
                    fileImg = "avatar.png";
                }
                else
                {
                    fileImg = Path.GetFileName(this.profileImage.PostedFile.FileName);
                    this.profileImage.SaveAs(Server.MapPath("~/UserProfileImages/") + fileImg);
                }
            }
            else
            {
                fileImg = "avatar.png";
            }

            string name = this.name_text.Text;
            int id = int.Parse(this.id_text.Text);
            string password = this.password_text.Text;

            string qry = "UPDATE user_tbl SET user_id={0}, firstname='{1}', password='{2}' ,profileImg='{3}' WHERE id=" + ((user_class.user_tup)(Session["user"])).Id + "";
            qry = string.Format(qry, id, name, password, "~/UserProfileImages/" + fileImg);

            user_class.user_tup user;
            if (SQL_Wrapper.SQL.UPDATE(qry))
            {
                user = new user_class.user_tup(((user_class.user_tup)(Session["user"])).Id, name, id, password, ((user_class.user_tup)(Session["user"])).Teacher, "~/UserProfileImages/" + fileImg, ((user_class.user_tup)(Session["user"])).Admin);
                Session["user"] = user;
            }
            load();
        }
    }
}