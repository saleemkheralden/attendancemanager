﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.WebSockets;
//using System.Net.Sockets;
using System.Threading;

namespace Manager.templates
{
    public partial class main_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("login_page.aspx");
            }
            else if (((user_class.user_tup)Session["user"]).Teacher)
            {
                string date = DateTime.Now.ToString("yyyy-MM-dd");
                this.user_name.Text = ((user_class.user_tup)(Session["user"])).Name;

                string qry = "SELECT * FROM lessons_tbl WHERE teacher_id=" + ((user_class.user_tup)(Session["user"])).User_id + " AND date='" + date + "' ORDER BY lesson_num ASC";

                DataSet lessons = SQL_Wrapper.SQL.SELECT(qry, "lessons_tbl");

                int length_lessons = lessons.Tables["lessons_tbl"].Rows.Count;

                for (int i = 0; i < length_lessons; i++)
                {
                    TableRow trow = new TableRow();
                    TableCell lesson_num = new TableCell();
                    TableCell subject = new TableCell();
                    TableCell _class = new TableCell();
                    lesson_num.Text = lessons.Tables["lessons_tbl"].Rows[i]["lesson_num"].ToString();

                    subject.Text = ((Dictionary<int, string>)(Session["subject"]))[int.Parse(lessons.Tables["lessons_tbl"].Rows[i]["subject"].ToString())];

                    HyperLink h = new HyperLink();
                    h.NavigateUrl = "list_page.aspx?id=" + lessons.Tables["lessons_tbl"].Rows[i]["class"].ToString() + "&lesson=" + lesson_num.Text;
                    h.Text = ((Dictionary<int, string>)(Session["class"]))[int.Parse(lessons.Tables["lessons_tbl"].Rows[i]["class"].ToString())];
                    h.ID = lessons.Tables["lessons_tbl"].Rows[i]["class"].ToString();

                    _class.Controls.Add(h);
                    trow.Controls.Add(lesson_num);
                    trow.Controls.Add(subject);
                    trow.Controls.Add(_class);
                    this.Lessons_tbl.Rows.Add(trow);
                }
            }
            else
            {

                //get in which class this students is in
                DataSet tempds = SQL_Wrapper.SQL.SELECT("SELECT id FROM school_students_tbl WHERE stu_id=" +
                ((user_class.user_tup)(Session["user"])).User_id, "school_students_tbl");

                int class_id = int.Parse(tempds.Tables["school_students_tbl"].Rows[0]["id"].ToString());


                //



                this.class_hc.Visible = false;
                this.nextWeek_link.Visible = false;
                this.user_name.Text = ((user_class.user_tup)(Session["user"])).Name;

                string day = DateTime.Today.DayOfWeek.ToString().ToLower();
                string sc_table = "weekly_sc_tbl";
                string sc_qry = "SELECT lesson, " + day + " FROM " + sc_table + " WHERE id=" + class_id + " ORDER BY lesson ASC";
                DataSet sc = SQL_Wrapper.SQL.SELECT(sc_qry, sc_table);

                string hour = DateTime.Now.ToString("HH:mm");

                for (int i = 0; i < sc.Tables[sc_table].Rows.Count; i++)
                {
                    TableRow trow = new TableRow();
                    TableCell lesson_num = new TableCell();
                    TableCell subject = new TableCell();


                    lesson_num.Text = sc.Tables[sc_table].Rows[i]["lesson"].ToString();
                    subject.Text = sc.Tables[sc_table].Rows[i][day].ToString();


                    if (subject.Text != "")
                    {
                        trow.Controls.Add(lesson_num);
                        trow.Controls.Add(subject);

                        if (check_absence(lesson_num.Text))
                            trow.Attributes.Add("style", "background-color:red");
                        else
                        {
                            string q_hour = "SELECT * FROM sc_hours_tbl";
                            DataSet hour_ds = SQL_Wrapper.SQL.SELECT(q_hour, "sc_hours_tbl");

                            for (int j = 0; j < hour_ds.Tables["sc_hours_tbl"].Rows.Count; j++)
                            {
                                string lesson_end = hour_ds.Tables["sc_hours_tbl"].Rows[int.Parse(lesson_num.Text)]["lesson_end"].ToString();
                                
                                if (DateTime.Compare(DateTime.Parse(hour), DateTime.Parse(lesson_end)) > 0)
                                    trow.Attributes.Add("style", "background-color:green");
                            }



                        }


                        trow.ID = lesson_num.Text;
                        this.Lessons_tbl.Rows.Add(trow);
                    }

                }

            }

            if (((user_class.user_tup)Session["user"]).Admin)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HyperLink adminLink = new HyperLink();
                adminLink.NavigateUrl = "admindash_page.aspx";
                adminLink.Text = "Admin dashboard";
                li.Controls.Add(adminLink);

                this.navbar.Controls.Add(li);
            }
        }

        protected bool check_absence(string lesson)
        {
            string q = "SELECT * FROM absence_tbl WHERE stu_id=" + ((user_class.user_tup)(Session["user"])).User_id + " AND date='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ORDER BY lesson ASC";
            DataSet absence_ds = SQL_Wrapper.SQL.SELECT(q, "absence_tbl");


            for (int i = 0; i < absence_ds.Tables["absence_tbl"].Rows.Count; i++)
                if (absence_ds.Tables["absence_tbl"].Rows[i]["lesson"].ToString() == lesson)
                    return true;

            return false;

        }
    }
}