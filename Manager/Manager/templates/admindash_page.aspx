﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admindash_page.aspx.cs" Inherits="Manager.templates.admindash_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="users">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="user_id" HeaderText="user_id" SortExpression="user_id" />
                    <asp:BoundField DataField="firstname" HeaderText="firstname" SortExpression="firstname" />
                    <asp:BoundField DataField="password" HeaderText="password" SortExpression="password" />
                    <asp:CheckBoxField DataField="teacher" HeaderText="teacher" SortExpression="teacher" />
                    <asp:BoundField DataField="profileImg" HeaderText="profileImg" SortExpression="profileImg" />
                    <asp:CheckBoxField DataField="admin" HeaderText="admin" SortExpression="admin" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="users" runat="server" ConnectionString="<%$ ConnectionStrings:WebSiteDBConnectionString %>" SelectCommand="SELECT * FROM [user_tbl]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
