﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="profile_page.aspx.cs" Inherits="Manager.templates.profile_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Profile</title>

    <script src="../static/javascript/SignupImgDisplay.js"></script>
    <link href="../static/css/profile_page_stylesheet.css" type="text/css" rel="stylesheet" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


</head>
<body>
    <form id="form1" runat="server">

         <div class="top-navbar">
                   <ul>
                       <li id="name_li">
                           <asp:Label ID="date" runat="server" Text=""></asp:Label>
                       </li>



                       <li style="float: right;"><a href="main_page.aspx">חזור</a></li>

                   </ul>
               </div>


        <div style="float: right; height: 132px; margin: 8px;">
           

            <asp:Table ID="profile_tbl" runat="server" dir="rtl" CssClass="table table-hover">
                
                <asp:TableRow>
                    <asp:TableCell>
                        <div id="img" runat="server" onclick="triggerClick()"></div>
                        <asp:FileUpload ID="profileImage" onchange="displayImg(this)" runat="server" style="display: none;"/>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow>
                    <asp:TableCell>
                        <span style="float: right">
                            שם
                        </span>
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:TextBox ID="name_text" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Width="100">
                        <span style="float: right">
                            תעודת זהות
                        </span>
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:TextBox ID="id_text" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <span style="float: right">
                            סיסמה
                        </span>
                    </asp:TableCell>

                    <asp:TableCell>
                        <asp:TextBox ID="password_text" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
            <br />


            <div style="float: right">
                <asp:Button ID="save_btn" runat="server" OnClick="save_btn_Click" Text="Save"/>
            </div>
            
            <asp:Label runat="server" ID="error_lbl"></asp:Label>

        </div>
    </form>
</body>
</html>
