﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Manager
{
    public partial class signup_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Image image = new Image();
                image.ImageUrl = "~/UserProfileImages/avatar.png";
                image.Width = 100;
                image.Height = 100;
                image.ID = "profileDisplay";

                this.img.Controls.Add(image);

                this.subject_list.Visible = false;
                string tbl = "subjects_label_tbl";
                string select_qry = "SELECT * FROM " + tbl;
                DataSet ds = SQL_Wrapper.SQL.SELECT(select_qry, tbl);

                for (int i = 0; i < ds.Tables[tbl].Rows.Count; i++)
                {
                    ListItem item = new ListItem(ds.Tables[tbl].Rows[i][1].ToString(), ds.Tables[tbl].Rows[i][0].ToString());
                    this.subject_list.Items.Add(item);
                }
            }
            else
            {
                string fileImg;
                if (this.profileImage.PostedFile != null)
                {
                    string path = Path.GetExtension(this.profileImage.PostedFile.FileName);
                    if (path != "")
                    {
                        if (path != ".jpg" && path != ".jpeg" && path != ".gif" && path != ".png")
                        {

                            this.error_msg_label.Text = "Only these image types allowed (.jpg, .jpeg, .gif, .png)";
                            fileImg = "avatar.png";
                        }
                        else
                        {
                            fileImg = Path.GetFileName(this.profileImage.PostedFile.FileName);
                            this.profileImage.SaveAs(Server.MapPath("~/UserProfileImages/") + fileImg);
                        }
                        Session["fileImg"] = fileImg;
                    }
                    else
                    {
                        fileImg = Session["fileImg"].ToString();
                    }
                }
                else
                {
                    fileImg = "avatar.png";
                }
                
                Image image = new Image();
                image.ImageUrl = "~/UserProfileImages/" + fileImg;
                image.Width = 100;
                image.Height = 100;
                image.ID = "profileDisplay";

                this.img.Controls.Add(image);

                this.password_textbox.Attributes.Add("VALUE", this.password_textbox.Text);
                this.password_conf_textbox.Attributes.Add("VALUE", this.password_conf_textbox.Text);
            }
        }

        protected void signup_btn_Click(object sender, EventArgs e)
        {

            string firstname = this.firstname_textbox.Text;
            int id;
            try
            {
                id = int.Parse(this.id_number_textbox.Text);
            }
            catch
            {
                this.error_msg_label.Text = "* Id field must be 9 digits";
                return;
            }
            string password = this.password_textbox.Text;
            bool temp_teacher = this.teacher_checkbox.Checked;
            int teacher;
            if (temp_teacher)
                teacher = 1;
            else
                teacher = 0;


            if (id.ToString().Length != 9)
            {
                this.error_msg_label.Text = "* Id must be 9 digits";
                
            }
            else if (password != this.password_conf_textbox.Text)
            {
                this.error_msg_label.Text = "* password and confirmation password doesn't match";
            }
            else
            {
                string qry_select = "SELECT * FROM user_tbl WHERE user_id=" + id;

                DataSet ds = SQL_Wrapper.SQL.SELECT(qry_select, "user_tbl");

                if (ds != null && ds.Tables["user_tbl"].Rows.Count == 0)
                {
                    if (temp_teacher)
                    {
                        string qry = string.Format("INSERT INTO user_tbl (user_id, firstname, password, teacher, profileImg, admin) VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5})", id, firstname, password, teacher, "~/UserProfileImages/" + Session["fileImg"].ToString(), 0);
                        SQL_Wrapper.SQL.INSERT(qry);

                        for (int i = 0; i < this.subject_list.Items.Count; i++)
                        {
                            if (this.subject_list.Items[i].Selected)
                            {
                                int subject_id = int.Parse(this.subject_list.Items[i].Value);

                                string sub_tbl = "subjects_tbl";
                                string sub_qry = "INSERT INTO " + sub_tbl + " (subject, teacher_id) VALUES ({0}, {1})";
                                sub_qry = string.Format(sub_qry, subject_id, id);

                                SQL_Wrapper.SQL.INSERT(sub_qry);
                            }
                        }

                        Response.Redirect("login_page.aspx");

                    }
                    else
                    {
                        string stu_qry = "SELECT * FROM school_students_tbl WHERE stu_id=" + id;

                        DataSet stu_ds = SQL_Wrapper.SQL.SELECT(stu_qry, "school_students_tbl");

                        if (stu_ds.Tables["school_students_tbl"].Rows.Count != 0)
                        {
                            string qry = string.Format("INSERT INTO user_tbl (user_id, firstname, password, teacher, profileImg, admin) VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5})", id, firstname, password, teacher, "~/UserProfileImages/" + Session["fileImg"].ToString(), 0);
                            SQL_Wrapper.SQL.INSERT(qry);

                            Response.Redirect("login_page.aspx");
                        }
                        else
                        {
                            this.error_msg_label.Text = "אתה לא רשום כתלמיד אנא בקש ממנהל המערכת להוסיף אותך";
                        }
                    }
                }
                else
                {
                    this.error_msg_label.Text = "* there's already account with this id";
                }
            }

        }

        protected void teacher_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.teacher_checkbox.Checked)
                this.subject_list.Visible = true;
            else
                this.subject_list.Visible = false;
        }
    }
}