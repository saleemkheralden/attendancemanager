﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="schedule_page.aspx.cs" Inherits="Manager.templates.schedule_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../static/css/schedule_page_stylesheet.css" type="text/css" rel="stylesheet" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>



</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="top-navbar">
                   <ul>
                       <li id="name_li">
                           <asp:Label ID="user_name" runat="server" Text=""></asp:Label>
                       </li>

                       <li>
                           <asp:DropDownList ID="date_droplist" AutoPostBack="true" runat="server" OnSelectedIndexChanged="date_droplist_SelectedIndexChanged">
                               <asp:ListItem Value="null">בחר תאריך</asp:ListItem>
                           </asp:DropDownList>
                       </li>

                       <li>
                           <asp:DropDownList ID="classes_droplist" runat="server" >
                               <asp:ListItem Value="null">בחר כיתה</asp:ListItem>
                           </asp:DropDownList>
                       </li>

                       <li>
                           <asp:DropDownList ID="subject_droplist" runat="server" >
                               <asp:ListItem Value="null">בחר מקצוע</asp:ListItem>
                           </asp:DropDownList>
                       </li>

                       <li>
                           <input type="number" min="1" max="10" id="lesson_num" runat="server"/>
                           <asp:Label ID="lesson_lbl" runat="server" Text=": שיעור"></asp:Label>
                       </li>

                       <li>
                           <asp:Button ID="add_lesson_btn" runat="server" Text="הוסף" AutoPostBack="true" OnClick="add_lesson_btn_Click" />
                       </li>
                       
                       <li>
                           <asp:Button ID="del_lesson_btn" runat="server" Text="מחק" AutoPostBack="true" OnClick="del_lesson_btn_Click" />
                       </li>

                       <li>
                           <asp:Label ID="err_lbl" runat="server" Text=""></asp:Label>
                       </li>

                       <li style="float: right;"><a href="main_page.aspx">חזור</a></li>
                       

                   </ul>
               </div>

            
            <asp:Table runat="server" ID="lessons" CssClass="table table-bordered">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell>
                        Class
                    </asp:TableHeaderCell>
                    
                    <asp:TableHeaderCell>
                        Subject
                    </asp:TableHeaderCell>

                    <asp:TableHeaderCell>
                        Lesson
                    </asp:TableHeaderCell>

                </asp:TableHeaderRow>
            </asp:Table>


            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server">
            </asp:GridView>
            <br />
            <br />
            <br />


        </div>
    </form>
</body>
</html>
