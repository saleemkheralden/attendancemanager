﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Data;

using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Manager.templates
{
    public partial class list_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["user"] == null)
                Response.Redirect("login_page.aspx");

            if (!IsPostBack)
                load();
            
            //if (Session["user"] == null)
            //{
            //    Response.Redirect("login_page.aspx");
            //}
            //else
            //{
            //    string date = DateTime.Now.ToString("dd/MM/yyyy");
            //    //ALTER TABLE school_students_tbl ADD "17/2/2020" nvarchar(50);

            //    string id = Request.QueryString["id"];




            //    string date_qry = "SELECT " + date + " FROM school_students_tbl WHERE id=" + id;

            //    DataSet date_ds = SQL_Wrapper.SQL.SELECT(date_qry, "school_students_tbl");

            //    this.Label1.Text = date;

            //    if (date_ds.Tables["school_students_tbl"].Rows.Count == 0)
            //    {
            //        SQL_Wrapper.SQL.addCol("[day" + date + "]");
            //        date_ds = SQL_Wrapper.SQL.SELECT(date_qry, "school_students_tbl");
            //    }

            //    string qry = "SELECT * FROM school_students_tbl WHERE id=" + id;

            //    DataSet list = SQL_Wrapper.SQL.SELECT(qry, "school_students_tbl");

            //    int length_list = list.Tables["school_students_tbl"].Rows.Count;

            //    for (int i = 0; i < length_list; i++)
            //    {
            //        TableRow trow = new TableRow();
            //        TableCell name = new TableCell();
            //        TableCell lastname = new TableCell();
            //        TableCell _class = new TableCell();
            //        TableCell user_id = new TableCell();
            //        TableCell checkBoxCell = new TableCell();
            //        TableCell overdueCheckBoxCell = new TableCell();


            //        CheckBox absenceCheckBox = new CheckBox();
            //        CheckBox overdueCheckBox = new CheckBox();


            //        name.Text = list.Tables["school_students_tbl"].Rows[i]["name"].ToString();
            //        lastname.Text = list.Tables["school_students_tbl"].Rows[i]["last_name"].ToString();
            //        user_id.Text = list.Tables["school_students_tbl"].Rows[i]["user_id"].ToString();
            //        _class.Text = list.Tables["school_students_tbl"].Rows[i]["id"].ToString();
            //        //lessons.Tables["lessons_tbl"].Rows[i]["class"].ToString();

            //        absenceCheckBox.ID = "ab:" + list.Tables["school_students_tbl"].Rows[i]["user_id"].ToString();
            //        absenceCheckBox.Text = "";
            //        absenceCheckBox.Checked = (date_ds.Tables["school_students_tbl"].Rows[i][0].ToString() == "True");
            //        absenceCheckBox.AutoPostBack = true;
            //        absenceCheckBox.Attributes.Add("OnCheckedChanged", "CheckBox_CheckedChanged");
            //        absenceCheckBox.Enabled = true;
            //        checkBoxCell.Controls.Add(absenceCheckBox);

            //        overdueCheckBox.ID = "od:" + list.Tables["school_students_tbl"].Rows[i]["user_id"].ToString();
            //        overdueCheckBox.Text = "";
            //        overdueCheckBox.AutoPostBack = true;
            //        overdueCheckBox.Checked = (date_ds.Tables["school_students_tbl"].Rows[i][0].ToString() == "True");
            //        overdueCheckBox.Attributes.Add("OnCheckedChanged", "CheckBox_CheckedChanged");
            //        overdueCheckBox.Enabled = true;



            //        overdueCheckBoxCell.Controls.Add(overdueCheckBox);



            //        trow.Controls.Add(name);
            //        trow.Controls.Add(lastname);
            //        trow.Controls.Add(_class);
            //        trow.Controls.Add(user_id);
            //        trow.Controls.Add(checkBoxCell);
            //        trow.Controls.Add(overdueCheckBoxCell);
            //        this.stu_lst.Rows.Add(trow);
            //    }
            //}
        }

        protected void load()
        {

            string date = DateTime.Now.ToString("yyyy-MM-dd");
            //ALTER TABLE school_students_tbl ADD "17/2/2020" nvarchar(50);

            string id = Request.QueryString["id"];
            string lesson = Request.QueryString["lesson"];

            this.class_lbl.Text = ((Dictionary<int, string>)(Session["class"]))[int.Parse(id)];

            //string date_qry = "SELECT [" + date + "] FROM school_students_tbl";

            //DataSet date_ds = SQL_Wrapper.SQL.SELECT(date_qry, "school_students_tbl");
            
            this.date_lbl.Text = date;

            //if (date_ds == null || date_ds.Tables["school_students_tbl"].Rows.Count == 0)
            //{
            //    SQL_Wrapper.SQL.addCol(date);
            //    date_ds = SQL_Wrapper.SQL.SELECT(date_qry, "school_students_tbl");
            //}

            string stu_lst_qry = "SELECT name, last_name, stu_id FROM school_students_tbl WHERE id=" + id + " ORDER BY name ASC";
            DataSet stu_lst = SQL_Wrapper.SQL.SELECT(stu_lst_qry, "school_students_tbl");

            string q = "SELECT * FROM absence_tbl WHERE teacher_id=" + ((user_class.user_tup)(Session["user"])).User_id + " AND date='" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND lesson=" + lesson;
            DataSet absence_ds = SQL_Wrapper.SQL.SELECT(q, "absence_tbl");

            stu_lst.Tables["school_students_tbl"].Columns.Add("absence", typeof(bool));

            for (int i = 0; i < stu_lst.Tables["school_students_tbl"].Rows.Count; i++)
            {
                bool flag = false;
                for (int index = 0; index < absence_ds.Tables["absence_tbl"].Rows.Count; index++)
                {
                    if (stu_lst.Tables["school_students_tbl"].Rows[i]["stu_id"].ToString()
                        == absence_ds.Tables["absence_tbl"].Rows[index]["stu_id"].ToString())
                    {
                        stu_lst.Tables["school_students_tbl"].Rows[i]["absence"] = true;
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                    stu_lst.Tables["school_students_tbl"].Rows[i]["absence"] = false;

            }



            //this.stu_tbl_gridView.Columns.Add()
            CheckBoxField field = (CheckBoxField)this.stu_tbl_gridView.Columns[3];
            field.DataField = "absence";
            field.HeaderText = date;

            this.stu_tbl_gridView.DataSource = stu_lst;

            this.GridView1.DataSource = absence_ds;
            
            try
            {
                this.stu_tbl_gridView.DataBind();
                this.GridView1.DataBind();
            }
            catch (Exception eErr)
            {
                this.update_check.Text = "bind is unsuccessful" + eErr.ToString();
            }
        }

        protected void GridView_RowCancelEditing(object sender, GridViewCancelEditEventArgs e)
        {
            this.stu_tbl_gridView.EditIndex = -1;
            load();
        }

        protected void GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            this.stu_tbl_gridView.EditIndex = e.NewEditIndex;
            load();
        }

        protected void GridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            GridViewRow row = (GridViewRow)this.stu_tbl_gridView.Rows[e.RowIndex];
            //TextBox txtname=(TextBox)gr.cell[].control[];
            int userid = int.Parse(((TextBox)(row.Cells[2].Controls[0])).Text);
            CheckBox checkAb = (CheckBox)row.Cells[3].Controls[0];
            //TextBox textadd = (TextBox)row.FindControl("txtadd");
            //TextBox textc = (TextBox)row.FindControl("txtc");  
            this.stu_tbl_gridView.EditIndex = -1;
            string lesson = Request.QueryString["lesson"];

            if (checkAb.Checked)
            {

                string select_qry = "SELECT * FROM absence_tbl WHERE stu_id=" + userid + " AND teacher_id=" + ((user_class.user_tup)(Session["user"])).User_id + " AND date='" + date + "' AND lesson=" + lesson;
                DataSet temp_ds = SQL_Wrapper.SQL.SELECT(select_qry, "absence_tbl");
                if (temp_ds.Tables["absence_tbl"].Rows.Count == 0)
                {
                    string absence_qry = "INSERT INTO absence_tbl (stu_id, teacher_id, date, lesson) VALUES ({0}, {1}, '{2}', {3})";
                    absence_qry = string.Format(absence_qry, userid, ((user_class.user_tup)(Session["user"])).User_id, date, lesson);
                    if (SQL_Wrapper.SQL.INSERT(absence_qry))
                        this.update_check.Text = "updated successfully";
                    else
                        this.update_check.Text = "updated failed";

                }

            }
            else
            {
                
                string absence_qry_del = "DELETE FROM absence_tbl WHERE stu_id={0} AND teacher_id={1} AND DATE='{2}' AND lesson={3}";
                absence_qry_del = string.Format(absence_qry_del, userid, ((user_class.user_tup)(Session["user"])).User_id, date, lesson);
                if (SQL_Wrapper.SQL.DELETE(absence_qry_del))
                    this.update_check.Text = "updated successfully";
                else
                    this.update_check.Text = "updated failed";

            }

            load();
            //GridView1.DataBind();  
        }

        protected string send()
        {

            string serverIP = "10.0.0.3";
            int Port = 54000;
            TcpClient client = new TcpClient(serverIP, Port);
            NetworkStream stream;

            byte[] bufferSend = new byte[2048];
            string dataSend;

            byte[] bufferRecv = new byte[100];
            string dataRecv;

            stream = client.GetStream();

            dataSend = Console.ReadLine();
            bufferSend = Encoding.ASCII.GetBytes(dataSend);
            stream.Write(bufferSend, 0, bufferSend.Length);
            bufferSend = new byte[2048];

            stream.Read(bufferRecv, 0, bufferRecv.Length);
            dataRecv = Encoding.ASCII.GetString(bufferRecv, 0, bufferRecv.Length);
            bufferRecv = new byte[100];

            return dataRecv;
        }
    }
}