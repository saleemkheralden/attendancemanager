﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login_page.aspx.cs" Inherits="Manager.login_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>

    <link rel="stylesheet" href="../static/css/login_page_stylesheet.css" type="text/css" />

   <%-- <link rel="stylesheet" href="../static/css/login_page_stylesheet.css" type="text/css" />
    <script src="../static/javascript/login_page_script.js"></script>--%>


    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/MorphSVGPlugin.min.js?r=182"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>


</head>
<body>

    <form id="login_page" runat="server">





        <div>

            <br />
            <asp:TextBox dir="rtl" ID="username_textbox" placeholder="שם משתמש" runat="server" ></asp:TextBox>
            <br />
            <br />
            <asp:TextBox dir="rtl" ID="password_textbox" placeholder="סיסמה" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            <asp:Button dir="rtl" runat="server" Text="התחבר" Width="67px" OnClick="login_btn_Click" ID="login_btn" />
            
            <br />
            <br />
            עדיין לא נרשמת?
            <a href="signup_page.aspx">לחץ כאן</a>

            <br />
            <br />
            <asp:Label ID="errorMsgLabel" runat="server"></asp:Label>
        </div>
        

        
    </form>
</body>
</html>
