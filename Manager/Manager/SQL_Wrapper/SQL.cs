﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//imports
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.SQL_Wrapper
{
    public class SQL
    {
        public SQL()
        {

        }

        public static DataSet SELECT(string qry, string TableName)
        {
            string ConnetionStr = ConfigurationManager.ConnectionStrings["WebSiteDB"].ConnectionString;

            SqlConnection connect = new SqlConnection(ConnetionStr);
            connect.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(qry, connect);
            DataSet ds = new DataSet();
            try
            {
                adapter.Fill(ds, TableName);
            }
            catch
            {
                return null;
            }
            connect.Close();
            return ds;

        }

        public static bool addCol(string colName)
        {
            string qry = "ALTER TABLE school_students_tbl ADD [" + colName + "] BIT";

            string ConnetionStr = ConfigurationManager.ConnectionStrings["WebSiteDB"].ConnectionString;
            SqlConnection connect = new SqlConnection(ConnetionStr);
            SqlCommand cmd = new SqlCommand(qry, connect);

            int SQLdone = 0;
            try
            {
                connect.Open();
                SQLdone = cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                connect.Close();
            }
        }

        public static bool col(string qry)
        {
            try
            {
                string ConnetionStr = ConfigurationManager.ConnectionStrings["WebSiteDB"].ConnectionString;

                SqlConnection connect = new SqlConnection(ConnetionStr);
                connect.Open();

                SqlCommand command = new SqlCommand(qry, connect);
                command.CommandText = qry;
                command.Connection = connect;
                command.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool INSERT(string qry)
        {
            return InnerFunc(qry);
        }

        public static bool DELETE(string qry)
        {
            return InnerFunc(qry);
        }

        public static bool UPDATE(string qry)
        {
            return InnerFunc(qry);
        }

        private static bool InnerFunc(string qry)
        {
            try
            {
                string ConnetionStr = ConfigurationManager.ConnectionStrings["WebSiteDB"].ConnectionString;
                SqlConnection connect = new SqlConnection(ConnetionStr);
                connect.Open();
                SqlCommand cmd = new SqlCommand(qry, connect);
                cmd.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}