﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Manager.Socket_manager
{
    public class Sockets
    {
        public string RecvServer(string ip, int port)
        {
            int Port = 5001;
            IPAddress IP = IPAddress.Parse("127.0.0.1");
            TcpListener server = new TcpListener(IP, Port);
            TcpClient client = new TcpClient();
            NetworkStream stream;

            server.Start();
            byte[] bufferSend = new byte[100];
            string dataSend;

            byte[] bufferRecv = new byte[100];
            string dataRecv;


            client = server.AcceptTcpClient();
            stream = client.GetStream();

            stream.Read(bufferRecv, 0, bufferRecv.Length);
            dataRecv = Encoding.ASCII.GetString(bufferRecv, 0, bufferRecv.Length);
            bufferRecv = new byte[100];

            bufferSend = Encoding.ASCII.GetBytes("server got that");
            dataSend = Encoding.ASCII.GetString(bufferSend, 0, bufferSend.Length);
            stream.Write(bufferSend, 0, bufferSend.Length);
            bufferSend = new byte[100];

            return dataRecv;

        }
    }
}